var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var swaggerUi = require('swagger-ui-express');
var swaggerJSDoc = require('swagger-jsdoc');

var indexRouter = require('./routes/index');
var customersRouter = require('./routes/customers');
var cartsRouter = require('./routes/carts');
var productsRouter = require('./routes/products');

var app = express();

// Database setup
var database = require('./models/database');
database.connect();

// Swagger setup
var swaggerSpec = swaggerJSDoc({
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'SHOP API',
            version: 'V1.0.0',
        },
    },
    apis: [
        './routes/index.js',
        './routes/customers.js',
        './routes/carts.js',
        './routes/products.js'
    ]
});
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerSpec, {explorer: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/customers', customersRouter);
app.use('/cart', cartsRouter);
app.use('/products', productsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    console.log(err);
    res.status(err.status || 500);
    res.json(err);
});

module.exports = app;
