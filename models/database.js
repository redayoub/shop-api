const MongoClient = require('mongodb').MongoClient;
var db;

module.exports.connect = () => {
    MongoClient
        .connect(process.env.DATABASE_URL || 'mongodb://mongodb:27017/shop')
        .then((client) => {
            db = client.db();
        })
        .catch((error) => {
            throw error;
        });
};;

module.exports.getDb = () => {    
    return db;
};