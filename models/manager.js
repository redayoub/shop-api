const database = require('./database');

class Manager {
    constructor(collectionName) {
        this.collectionName = collectionName;
    }

    findBy(filter = {}) {
        return database.getDb()
            .collection(this.collectionName)
            .find(filter)
            .toArray()
            .then(results => {
                return results;
            })
            .catch((error) => {
                throw error;
            });
    }

    findOneBy(filter) {
        return database.getDb()
            .collection(this.collectionName)
            .findOne(filter)
            .then(result => {
                return result;
            })
            .catch((error) => {
                throw error;
            });
    }

    create(data) {
        return database.getDb()
            .collection(this.collectionName)
            .insertOne(data)
            .then((result) => {
                return result.ops[0];
            })
            .catch((error) => {
                throw error;
            });
    }

    updateOneBy(filter, data) {
        return database.getDb()
            .collection(this.collectionName)
            .findOneAndUpdate(
                filter, 
                {$set: data}
            )
            .then((result) => {
                return result;
            })
            .catch((error) => {
                throw error;
            });
    }

    deleteOneBy(filter) {
        return database.getDb()
            .collection(this.collectionName)
            .deleteOne(filter)
            .then((result) => {
                return result;
            })
            .catch((error) => {
                throw error;
            });
    }
}

module.exports = Manager;