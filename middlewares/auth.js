var jwt = require('jsonwebtoken');
var key = require('../data/key');

module.exports.verifyToken = (req, res, next) => {
    let token = req.headers['authorization'];
    if (token && token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
    }

    if (token) {
        jwt.verify(token, key.secret, (error, decoded) => {
            if (error) {
                return res.status(401).json({
                    error: 'Access token is missing or invalid'
                });
            } else {
                req.loggedUserData = decoded;
                next();
            }
        });
    } else {
        return res.status(401).json({
            error: 'Access token is missing or invalid'
        });
    }
};

