let chai = require('chai');
let chaiHttp = require('chai-http');
var expect = chai.expect;

chai.use(chaiHttp);

describe('/GET products', () => {
    it('it should GET all the products', (done) => {
        chai.request('http://localhost:3000')
            .get('/products')
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
});
