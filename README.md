# SHOP API

## Requirements

- git
- docker
- docker-compose

## Install The App Locally

```bash
git clone git@gitlab.com:redayoub/shop-api.git
cd shop-api/
make install
```

## Run The App Locally

```bash
cd shop-api/
make run
```

## Run Load test

```bash
cd shop-api/
make loadtest
```