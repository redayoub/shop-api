var express = require('express');
var router = express.Router();
const { check } = require('express-validator');
const customerController = require('../controllers/customers');

router.get('/', function(req, res, next) {
    res.json({
        name: "SHOP API",
        version: "V1.0.0",
        documentation: "http://localhost:3000/doc"
    });
});

/**
 * @swagger
 *
 * /login:
 *   post:
 *     summary: Login
 *     tags:
 *       - Customer
 *     produces:
 *       - application/json
 *     requestBody:
 *         description: Login Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 email:
 *                   type: string
 *                 password:
 *                   type: string
 *             example:
 *               email: email@gmail.com
 *               password: test1234
 *     responses:
 *       200:
 *         description: Return JWT token
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       400:
 *         description: Bad credentails
 */
router.post(
    '/login', 
    [
        check('email').isEmail().normalizeEmail()
    ],
    customerController.login
);

module.exports = router;