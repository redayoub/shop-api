var express = require('express');
var router = express.Router();
var cartController = require('../controllers/carts');
var verifyToken = require('../middlewares/auth').verifyToken;

/**
 * @swagger
 * 
 * components:
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 * 
 *   schemas:
 *     Cart:
 *       type: object
 *       properties:
 *         items:
 *           type: object
 *         totalPrice:
 *           type: number
 *
 *     CartData:
 *       type: object
 *       properties:
 *         productId:
 *           type: string
 * 
 *   responses:
 *     UnauthorizedError:
 *       description: Access token is missing or invalid
 */

/**
 * @swagger
 *
 * /cart:
 *   get:
 *     summary: Get logged in user cart
 *     tags:
 *       - Cart
 *     produces:
 *       - application/json
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Return cart
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Cart'
 *       401:
 *         $ref: '#/components/responses/UnauthorizedError'
 */
router.get('/', verifyToken, cartController.getCart);

/**
 * @swagger
 *
 * /cart/addProduct:
 *   post:
 *     summary: Add product to cart
 *     tags:
 *       - Cart
 *     requestBody:
 *         description: Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CartData'
 *             example:
 *               productId: 5d2062bdcb6177027e60d63a
 *     produces:
 *       - application/json
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         summary: Return cart
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Cart'
 *       401:
 *         $ref: '#/components/responses/UnauthorizedError'
 */
router.post('/addProduct', verifyToken, cartController.addProduct);

/**
 * @swagger
 *
 * /cart/removeProduct:
 *   post:
 *     summary: Remove product from cart
 *     tags:
 *       - Cart
 *     requestBody:
 *         description: Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CartData'
 *             example:
 *               productId: 5d2062bdcb6177027e60d63a
 *     produces:
 *       - application/json
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         summary: Return cart
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Cart'
 *       401:
 *         $ref: '#/components/responses/UnauthorizedError'
 */
router.post('/removeProduct', verifyToken, cartController.removeProduct);

module.exports = router;