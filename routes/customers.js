var express = require('express');
var router = express.Router();
var { check } = require('express-validator');
var customerController = require('../controllers/customers');
var verifyToken = require('../middlewares/auth').verifyToken;

/**
 * @swagger
 * 
 * components:
 *   schemas:
 *     Cart:
 *       type: object
 *       properties:
 *         items:
 *           type: object
 *         totalPrice:
 *           type: number
 * 
 *     Customer:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         email:
 *           type: string
 *         password:
 *           type: string
 *         cart:
 *           type: object
 *           schema:
 *               $ref: '#/components/schemas/Cart'
 * 
 *     CustomerData:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *         email:
 *           type: string
 *         password:
 *           type: string
 */

/**
 * @swagger
 *
 * /customers:
 *   post:
 *     summary: Create customer
 *     tags:
 *       - Customer
 *     produces:
 *       - application/json
 *     requestBody:
 *         description: Customer Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CustomerData'
 *             example:
 *               name: Ayoub REDA
 *               email: email@gmail.com
 *               password: test1234
 *     responses:
 *       201:
 *         description: Return customer
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Customer'
 *       400:
 *         description: Customer already exists
 */
router.post(
    '/', 
    [
        check('name').isLength({min: 2, max: 15}),
        check('email').isEmail().normalizeEmail(),
        check('password').isLength({min: 8, max: 15})
    ],
    customerController.createCustomer
);

/**
 * @swagger
 *
 * /customers/{customerId}:
 *   put:
 *     summary: Update customer
 *     tags:
 *       - Customer
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: customerId
 *         schema:
 *           type: string
 *           required: true
 *         description: Customer ID
 *     requestBody:
 *         description: Customer Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CustomerData'
 *             example:
 *               name: Ayoub REDA
 *     responses:
 *       200:
 *         description: Return customer
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Customer'
 *       404:
 *         description: Customer not found
 */
router.put(
    '/:customerId', 
    [
        check('name').isLength({min: 2, max: 15})
    ],
    customerController.updateCustomer
);

module.exports = router;