var express = require('express');
var router = express.Router();
var { check } = require('express-validator');
var productController = require('../controllers/products');

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     Product:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         price:
 *           type: number
 *         quantity:
 *           type: integer
 * 
 *     ProductData:
 *       type: object
 *       required:
 *         - name
 *         - price
 *         - quantity
 *       properties:
 *         name:
 *           type: string
 *         price:
 *           type: number
 *         quantity:
 *           type: integer
 * 
 *     ProductUpdateData:
 *       type: object
 *       required:
 *         - price
 *         - quantity
 *       properties:
 *         price:
 *           type: number
 *         quantity:
 *           type: integer
 */

/**
 * @swagger
 *
 * /products:
 *   get:
 *     summary: Get products
 *     tags:
 *       - Product
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get products
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Product'
 */
router.get('/', productController.getProducts);

/**
 * @swagger
 *
 * /products:
 *   post:
 *     summary: Create product
 *     tags:
 *       - Product
 *     produces:
 *       - application/json
 *     requestBody:
 *         description: Product Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ProductData'
 *             example:
 *               name: Product 1
 *               quantity: 123
 *               price: 12.34
 *     responses:
 *       201:
 *         description: Return product
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Product'
 *       400:
 *         description: Product already exists
 */
router.post(
    '/', 
    [
        check('name').isLength({min: 2, max: 15}),
        check('quantity').isNumeric(),
        check('price').isNumeric()
    ],
    productController.createProduct
);

/**
 * @swagger
 *
 * /products/{productId}:
 *   get:
 *     summary: Get product
 *     tags:
 *       - Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: productId
 *         schema:
 *           type: string
 *           required: true
 *         description: Product ID
 *     responses:
 *       200:
 *         description: Return product
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Product'
 *       400:
 *         description: Product not found
 */
router.get('/:productId', productController.getProduct);

/**
 * @swagger
 *
 * /products/{productId}:
 *   put:
 *     summary: Update product
 *     tags:
 *       - Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: productId
 *         schema:
 *           type: string
 *           required: true
 *         description: Product ID
 *     requestBody:
 *         description: Product Data
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ProductUpdateData'
 *             example:
 *               quantity: 123
 *               price: 12.34
 *     responses:
 *       200:
 *         description: Return product
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Product'
 *       400:
 *         description: Product not found
 */
router.put(
    '/:productId', 
    [
        check('quantity').isNumeric(),
        check('price').isNumeric()
    ],
    productController.updateProduct
);

/**
 * @swagger
 *
 * /products/{productId}:
 *   delete:
 *     summary: Delete product
 *     tags:
 *       - Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: productId
 *         schema:
 *           type: string
 *           required: true
 *         description: Product ID
 *     responses:
 *       204:
 *         description: Return no content
 */
router.delete('/:productId', productController.removeProduct);

module.exports = router;