
var mongodb = require('mongodb');
var Manager = require('../models/manager');
var customerManager = new Manager('customers');
var productManager = new Manager('products');

/**
 * Get Cart.
 */
module.exports.getCart = (req, res, next) => {
    customerManager
        .findOneBy({
            _id: new mongodb.ObjectID(req.loggedUserData.id)
        })
        .then((customer) => {            
            if (customer) {
                res.json(customer.cart);
            } else {
                res.status(404).json({
                    error: "Customer not found"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};

/**
 * Add Product.
 */
module.exports.addProduct = (req, res, next) => {
    customerManager
        .findOneBy({
            _id: new mongodb.ObjectID(req.loggedUserData.id)
        })
        .then((customer) => {            
            if (customer) {
                let { productId } = req.body;

                productManager
                    .findOneBy({
                        _id: new mongodb.ObjectID(productId)
                    })
                    .then((product) => {
                        if (product) {
                            let { items } = customer.cart;
                            if (!items[productId]) {
                                items[productId] = {
                                    name: product.name,
                                    price: product.price,
                                    quantity: 1
                                };
                            } else {
                                items[productId] = {
                                    ...items[productId],
                                    quantity: items[productId].quantity + 1
                                }
                            }

                            let totalPrice = 0;
                            Object.keys(items).forEach(key => {
                                totalPrice += items[key].quantity * items[key].price;
                            });

                            let cart = {
                                items: items,
                                totalPrice: totalPrice
                            };

                            customerManager
                                .updateOneBy(
                                    {
                                        _id: new mongodb.ObjectId(req.loggedUserData.id)
                                    }, {
                                        cart: cart
                                    }
                                )
                                .then((result) => {
                                    res.json(cart);
                                })
                                .catch((error) => {
                                    next(error);
                                });
                        } else {
                            res.status(404).json({
                                error: "Product not found"
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            } else {
                res.status(404).json({
                    error: "Customer not found"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};

/**
 * Remove Product.
 */
module.exports.removeProduct = (req, res, next) => {
    customerManager
        .findOneBy({
            _id: new mongodb.ObjectID(req.loggedUserData.id)
        })
        .then((customer) => {            
            if (customer) {
                let { productId } = req.body;
                let { items } = customer.cart;
                
                if (items[productId] && items[productId].quantity >= 1) {
                    items[productId] = {
                        ...items[productId],
                        quantity: items[productId].quantity - 1
                    };

                    let totalPrice = 0;
                    Object.keys(items).forEach(key => {
                        totalPrice += items[key].quantity * items[key].price;
                    });

                    let cart = {
                        items: items,
                        totalPrice: totalPrice
                    };

                    if (items[productId].quantity == 0) {
                        delete items[productId];
                    }
    
                    customerManager
                        .updateOneBy(
                            {
                                _id: new mongodb.ObjectId(req.loggedUserData.id)
                            }, {
                                cart: cart
                            }
                        )
                        .then((customer) => {  
                            res.json(cart);
                        })
                        .catch((error) => {
                            next(error);
                        }); 
                } else {
                    res.json(customer.cart);
                }
            } else {
                res.status(404).json({
                    error: "Customer not found"
                });
            }
        });
};