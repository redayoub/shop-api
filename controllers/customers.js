
var mongodb = require('mongodb');
const { validationResult } = require('express-validator');
var bcrypt = require('bcryptjs');
var Manager = require('../models/manager');
var customerManager = new Manager('customers');
var jwt = require('jsonwebtoken');
var key = require('../data/key');

module.exports.createCustomer = (req, res, next) => {
    var { name, email, password } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    customerManager
        .findOneBy({
            email: email
        })
        .then((customer) => {
            if (!customer) {
                bcrypt
                    .hash(password, 12)
                    .then((password) => {
                        customerManager
                            .create({
                                name: name, 
                                email: email,
                                password: password,
                                cart: {
                                    items: {},
                                    totalPrice: 0
                                }
                            })
                            .then((customer) => {   
                                delete customer.password;                     
                                res.status(201).json(customer);
                            })
                            .catch((error) => {
                                next(error);
                            });
                    })
                    .catch((error) => {
                        next(error);
                    });
            } else {
                res.status(400).json({
                    error: "Customer with same email already exists"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};

module.exports.updateCustomer = (req, res, next) => {
    var { name } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    customerManager
        .updateOneBy(
            {
                _id: new mongodb.ObjectId(req.params.customerId)
            }, {
                name: name
            }
        )
        .then((result) => {
            if (result.value) {
                delete result.value.password;
                res.json(result.value);
            } else {
                res.status(404).json({
                    error: "Customer not found"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};

module.exports.login = (req, res, next) => {
    var { email, password } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    customerManager
        .findOneBy({email: email})
        .then((customer) => {
            if (customer) {
                bcrypt
                    .compare(password, customer.password)
                    .then((match) => {
                        if (match) {
                            let token = jwt.sign(
                                {id: customer._id},
                                key.secret,
                                {expiresIn: '1h'}
                            );
                            
                            delete customer.password;
                            res.json({
                                token
                            });
                        } else {
                            res.status(404).json({
                                error: "Bad credentials"
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            } else {
                res.status(404).json({
                    error: "Bad credentials"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};