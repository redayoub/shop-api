
var mongodb = require('mongodb');
var Manager = require('../models/manager');
var productManager = new Manager('products');
const { validationResult } = require('express-validator');

/**
 * Get Products.
 */
module.exports.getProducts = (req, res, next) => {
    productManager
        .findBy()
        .then((products) => {
            res.json(products);
        })
        .catch((error) => {
            next(error);
        });
};

/**
 * Create Product.
 */
module.exports.createProduct = (req, res, next) => {
    var { name, price, quantity } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    productManager
        .findOneBy({
            name: name
        })
        .then((product) => {
            if (!product) {
                productManager
                    .create({
                        name: name, 
                        price: price,
                        quantity: quantity
                    })
                    .then((product) => {                        
                        res.status(201).json(product);
                    })
                    .catch((error) => {
                        next(error);
                    });
            } else {
                res.status(400).json({
                    error: "Product already exists"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};

/**
 * Get Product
 */
module.exports.getProduct = (req, res, next) => {
    productManager
        .findOneBy({
            _id: new mongodb.ObjectID(req.params.productId)
        })
        .then((product) => {            
            if (product) {
                res.json(product);
            } else {
                res.status(404).json({
                    error: "Product not found"
                });
            }
        })
        .catch((error) => {
            next(error);
        });
};

/**
 * Update Product.
 */
module.exports.updateProduct = (req, res, next) => {
    var { quantity, price } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    productManager
        .updateOneBy(
            {
                _id: new mongodb.ObjectId(req.params.productId)
            }, {
                quantity: quantity, 
                price: price
            }
        )
        .then((result) => {  
            if (result.value) {
                res.json(result.value);
            } else {
                res.status(404).json({
                    error: "Product not found"
                });
            }                      
        })
        .catch((error) => {
            next(error);
        });
};

/**
 * Remove Product.
 */
module.exports.removeProduct = (req, res, next) => {
    productManager
        .deleteOneBy({
            _id: new mongodb.ObjectId(req.params.productId)
        })
        .then((result) => {
            res.status(204).json('');
        })
        .catch((error) => {
            next(error);
        });
};