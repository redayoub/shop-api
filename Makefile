.DEFAULT_GOAL := help
.PHONY: install ssh run loadtest

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## Project setup
##---------------------------------------------------------------------------

install: node_modules

node_modules: package.json
	docker-compose run node npm install
	docker-compose run node touch node_modules

##
## APP
##---------------------------------------------------------------------------

ssh:																						
	docker-compose exec node bash

run:
	docker-compose up

loadtest:
	docker-compose run node artillery run /app/loadtest.yaml