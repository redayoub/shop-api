FROM node:12.4.0

WORKDIR /app

COPY package*.json ./

ARG BRANCH_NAME 
ARG DATABASE_URL

RUN if [ "$BRANCH_NAME" != "develop" ]; \
    then \
        NODE_ENV=production; \
    fi

RUN DATABASE_URL=$DATABASE_URL npm install && npm install pm2 -g

COPY . .

RUN chmod -R 777 /app /app/*

EXPOSE 80
CMD ["npm", "start"]